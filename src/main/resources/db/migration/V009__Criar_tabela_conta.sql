CREATE TABLE Conta(
	id				SERIAL PRIMARY KEY ,
	valor_conta		DOUBLE PRECISION 	NOT NULL,
	data_pagamento	DATE 				NOT NULL,
	descricao		VARCHAR(200) 		NOT NULL
);