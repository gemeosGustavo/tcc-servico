CREATE TABLE Dizimo(
	id				SERIAL PRIMARY KEY ,
	valor_dizimo		DOUBLE PRECISION	NOT NULL,
	data_dizimo		DATE 				NOT NULL,
	pessoaid		INTEGER 			NOT NULL,
	FOREIGN KEY (pessoaid) REFERENCES Pessoa(id)
);
