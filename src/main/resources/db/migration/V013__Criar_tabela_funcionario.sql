CREATE TABLE Funcionario(
	id				SERIAL PRIMARY KEY ,
	cargo			VARCHAR(50)			NOT NULL,
	salario			DOUBLE PRECISION 	NOT NULL,
	pessoaid		INTEGER 			NOT NULL,
	FOREIGN KEY (pessoaid) REFERENCES Pessoa(id)
);
