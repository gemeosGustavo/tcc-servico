CREATE TABLE Salario(
	id						SERIAL PRIMARY KEY,
	valor_pagamento			DOUBLE PRECISION 	NOT NULL,
	data_pagamento_salario	DATE 				NOT NULL,
	descricao_pagamento		VARCHAR(200) 		NOT NULL,
	funcionarioid			INTEGER 			NOT NULL,
	FOREIGN KEY (funcionarioid) REFERENCES Funcionario(id)
);