CREATE TABLE Usuario(
	id SERIAL PRIMARY KEY,
	nome			VARCHAR(50)		NOT NULL,
	sobrenome		VARCHAR(50)		NOT NULL,
	username		VARCHAR(50)		NOT NULL,
	password		VARCHAR(600)	NOT NULL,
	permissao		INTEGER			NOT NULL
);
