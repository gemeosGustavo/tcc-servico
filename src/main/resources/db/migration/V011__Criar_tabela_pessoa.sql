CREATE TABLE Pessoa(
	id				SERIAL PRIMARY KEY ,
	nome			VARCHAR(200)	NOT NULL,
	telefone		VARCHAR(50)		NOT NULL,
	data_nascimento	DATE 			NOT NULL,
	numero_casa		INTEGER			NOT NULL,
	enderecoid		INTEGER 		NOT NULL,
	excluido 		INTEGER			NOT NULL 	DEFAULT 0,
	FOREIGN KEY (enderecoid) REFERENCES Endereco(id)
);
