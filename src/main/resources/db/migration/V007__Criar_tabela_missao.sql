CREATE TABLE Missao(
	id				SERIAL PRIMARY KEY ,
	valor_missao		DOUBLE PRECISION	NOT NULL,
	data_oferta_missao	DATE 	NOT NULL
);