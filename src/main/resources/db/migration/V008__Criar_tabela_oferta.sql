CREATE TABLE Oferta(
	id				SERIAL PRIMARY KEY ,
	valor_oferta		DOUBLE PRECISION	NOT NULL,
	data_oferta		DATE 				NOT NULL
);