CREATE TABLE usuario_roles(
	usuariorolesid				SERIAL PRIMARY KEY ,
	usuario		INTEGER			NOT NULL,
	role		INTEGER			NOT NULL,
	FOREIGN KEY (usuario) REFERENCES Usuario(id),
	FOREIGN KEY (role) REFERENCES Role(id)
);
