package com.tcc.sgfr.security.services;

import java.util.List;

import com.tcc.sgfr.domain.model.Usuario;

public interface UserService{
	
	Usuario save(Usuario user);
	List<Usuario> findAll();
	void delete(long id);
	Usuario findOne(String username);
	Usuario updatePass(long id , Usuario user);
	Usuario findById(Long id);

}
