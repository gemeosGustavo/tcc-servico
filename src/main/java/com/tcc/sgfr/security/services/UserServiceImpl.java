package com.tcc.sgfr.security.services;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.tcc.sgfr.domain.model.Usuario;
import com.tcc.sgfr.domain.repository.UsuarioRepository;
import com.tcc.sgfr.security.services.UserService;

@Service("userService")
public class UserServiceImpl implements UserDetailsService, UserService {
	
	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	 
	@Override
	public Usuario save(Usuario user) {
	
		Usuario newUsuario = new Usuario();
		newUsuario.setNome(user.getNome());
		newUsuario.setSobrenome(user.getSobrenome());
		newUsuario.setUsername(user.getUsername());
		newUsuario.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		newUsuario.setPermissao(user.getPermissao());
		System.out.println(newUsuario);
		return usuarioRepository.save(newUsuario);
		
	}
	  
	@Override
	public List<Usuario> findAll() {
		List<Usuario> list = new ArrayList<>();
		usuarioRepository.findAll().iterator().forEachRemaining(list::add);
		return list;
	}

	@Override
	public void delete(long id) {
		usuarioRepository.delete(id);

	}

	@Override
	public Usuario findOne(String username) {
		return usuarioRepository.findByUsername(username);
	}

	@Override
	public Usuario updatePass(long id, Usuario user) {
		Usuario updateUsuario = new Usuario();
		return null;
	}

	@Override
	public Usuario findById(Long id) {
		return usuarioRepository.findOne(id);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario usuario = usuarioRepository.findByUsername(username);
		if (usuario == null) {
			throw new UsernameNotFoundException("Usuario ou senha invalida!!");
		}
		return new User(usuario.getUsername(), usuario.getPassword(), getAuthority(usuario));
	}

	private Set<SimpleGrantedAuthority> getAuthority(Usuario usuario) {
		Set<SimpleGrantedAuthority> authorities = new HashSet<>();
		usuario.getRoles().forEach(role -> {
			authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getName()));
		});
		return authorities;
	}

}
