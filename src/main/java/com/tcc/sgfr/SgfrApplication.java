package com.tcc.sgfr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SgfrApplication {

	public static void main(String[] args) {
		SpringApplication.run(SgfrApplication.class, args);
	}

}
