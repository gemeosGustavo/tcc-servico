package com.tcc.sgfr.domain.repository.query;

import java.time.LocalDate;
import java.util.List;

public interface SalarioRepositoryQuery {
	
	public Object valorPagamento(LocalDate data_inicio, LocalDate data_fim);
	public List<Object> salarios(LocalDate data_inicio, LocalDate date_fim);

}
