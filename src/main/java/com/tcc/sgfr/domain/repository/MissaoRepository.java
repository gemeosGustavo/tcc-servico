package com.tcc.sgfr.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tcc.sgfr.domain.model.Missao;
import com.tcc.sgfr.domain.repository.query.MissaoRepositoryQuery;

@Repository
public interface MissaoRepository extends JpaRepository<Missao, Long>, MissaoRepositoryQuery{

}
