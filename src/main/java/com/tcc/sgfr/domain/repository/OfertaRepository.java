package com.tcc.sgfr.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tcc.sgfr.domain.model.Oferta;
import com.tcc.sgfr.domain.repository.query.OfertaRepositoryQuery;

@Repository
public interface OfertaRepository extends JpaRepository<Oferta, Long>, OfertaRepositoryQuery {

}
