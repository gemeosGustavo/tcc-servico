package com.tcc.sgfr.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tcc.sgfr.domain.model.Dizimo;
import com.tcc.sgfr.domain.repository.query.DizimoRepositoryQuery;

@Repository
public interface DizimoRepository extends JpaRepository<Dizimo, Long>, DizimoRepositoryQuery{

}
