package com.tcc.sgfr.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tcc.sgfr.domain.model.Conta;
import com.tcc.sgfr.domain.repository.query.ContaRepositoryQuery;


@Repository
public interface ContaRepository extends JpaRepository<Conta, Long>, ContaRepositoryQuery {

}

