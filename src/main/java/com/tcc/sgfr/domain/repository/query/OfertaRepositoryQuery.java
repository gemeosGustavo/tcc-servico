package com.tcc.sgfr.domain.repository.query;

import java.time.LocalDate;
import java.util.List;

public interface OfertaRepositoryQuery {
	
	public Object valorOferta(LocalDate data_inicio, LocalDate data_fim);
	public List<Object> ofertas(LocalDate data_inicio, LocalDate date_fim);

}
