package com.tcc.sgfr.domain.repository.implementation;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import com.tcc.sgfr.domain.repository.query.ContaRepositoryQuery;

public class ContaRepositoryImpl implements ContaRepositoryQuery{

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Object valorConta(LocalDate data_inicio, LocalDate data_fim) {
		StringBuilder consulta = new StringBuilder();
		
		consulta.append("SELECT sum(conta.valor_conta) ")
		.append("FROM Conta conta ")
		.append("WHERE conta.data_pagamento BETWEEN :data_inicio AND :data_fim");
	
		try {
			return manager.createQuery(consulta.toString(),Object.class)
						  .setParameter("data_inicio", data_inicio)
						  .setParameter("data_fim", data_fim).getSingleResult();
		} catch(NoResultException erro){
			return null;
		}
		
	}
	
	@Override
	public List<Object> contas(LocalDate data_inicio, LocalDate data_fim) {
		StringBuilder consulta = new StringBuilder();
		
		consulta.append("SELECT conta ")
		.append("FROM Conta conta ")
		.append("WHERE conta.data_pagamento BETWEEN :data_inicio AND :data_fim");
	
		try {
			return manager.createQuery(consulta.toString(),Object.class)
						  .setParameter("data_inicio", data_inicio)
						  .setParameter("data_fim", data_fim).getResultList();
		} catch(NoResultException erro){
			return null;
		}
		
	}
	

}
