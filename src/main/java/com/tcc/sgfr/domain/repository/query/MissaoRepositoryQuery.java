package com.tcc.sgfr.domain.repository.query;

import java.time.LocalDate;
import java.util.List;

public interface MissaoRepositoryQuery {
	
	public Object valorMissao(LocalDate data_inicio, LocalDate data_fim);
	public List<Object> missoes(LocalDate data_inicio, LocalDate date_fim);

}
