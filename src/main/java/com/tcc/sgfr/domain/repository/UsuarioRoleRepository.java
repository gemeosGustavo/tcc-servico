package com.tcc.sgfr.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tcc.sgfr.domain.model.UsuarioRole;

public interface UsuarioRoleRepository extends JpaRepository<UsuarioRole, Long> {

}
