package com.tcc.sgfr.domain.repository.implementation;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import com.tcc.sgfr.domain.repository.query.MissaoRepositoryQuery;

public class MissaoRepositoryImpl implements MissaoRepositoryQuery{

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Object valorMissao(LocalDate data_inicio, LocalDate data_fim){
		StringBuilder consulta = new StringBuilder();
		
		consulta.append("SELECT sum(missao.valor_missao) ")
		.append("FROM Missao missao ")
		.append("WHERE missao.data_oferta_missao BETWEEN :data_inicio AND :data_fim ");
	
		try {
			return manager.createQuery(consulta.toString(),Object.class)
						  .setParameter("data_inicio", data_inicio)
						  .setParameter("data_fim", data_fim).getResultList();
		} catch(NoResultException erro){
			return null;
		}
		
	}
	
	@Override
	public List<Object> missoes(LocalDate data_inicio, LocalDate data_fim) {
		StringBuilder consulta = new StringBuilder();
		
		consulta.append("SELECT missao ")
		.append("FROM Missao missao ")
		.append("WHERE missao.data_oferta_missao BETWEEN :data_inicio AND :data_fim");
	
		try {
			return manager.createQuery(consulta.toString(),Object.class)
						  .setParameter("data_inicio", data_inicio)
						  .setParameter("data_fim", data_fim).getResultList();
		} catch(NoResultException erro){
			return null;
		}
		
	}
	
}
