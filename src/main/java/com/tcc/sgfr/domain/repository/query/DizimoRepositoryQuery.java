package com.tcc.sgfr.domain.repository.query;

import java.time.LocalDate;
import java.util.List;

public interface DizimoRepositoryQuery {

	public Object valorDizimo(LocalDate data_inicio, LocalDate data_fim);
	public List<Object> dizimos(LocalDate data_inicio, LocalDate date_fim);
}
