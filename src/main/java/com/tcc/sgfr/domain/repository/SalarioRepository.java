package com.tcc.sgfr.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tcc.sgfr.domain.model.Salario;
import com.tcc.sgfr.domain.repository.query.SalarioRepositoryQuery;

@Repository
public interface SalarioRepository extends JpaRepository<Salario, Long>, SalarioRepositoryQuery{

}
