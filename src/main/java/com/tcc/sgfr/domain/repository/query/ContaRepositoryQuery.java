package com.tcc.sgfr.domain.repository.query;

import java.time.LocalDate;
import java.util.List;

public interface ContaRepositoryQuery {
	
	public Object valorConta(LocalDate data_inicio, LocalDate data_fim);
	public List<Object> contas(LocalDate data_inicio, LocalDate date_fim);

}
