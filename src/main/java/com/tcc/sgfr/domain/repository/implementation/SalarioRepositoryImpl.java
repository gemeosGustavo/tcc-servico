package com.tcc.sgfr.domain.repository.implementation;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import com.tcc.sgfr.domain.repository.query.SalarioRepositoryQuery;

public class SalarioRepositoryImpl implements SalarioRepositoryQuery{
	
	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Object valorPagamento(LocalDate data_inicio, LocalDate data_fim){
		StringBuilder consulta = new StringBuilder();
		
		consulta.append("SELECT sum(salario.valor_pagamento) ")
		.append("FROM Salario salario ")
		.append("WHERE salario.data_pagamento_salario BETWEEN :data_inicio AND :data_fim ");
	
		try {
			return manager.createQuery(consulta.toString(),Object.class)
						  .setParameter("data_inicio", data_inicio)
						  .setParameter("data_fim", data_fim).getSingleResult();
		} catch(NoResultException erro){
			return null;
		}
		
	}
	
	@Override
	public List<Object> salarios(LocalDate data_inicio, LocalDate data_fim) {
		StringBuilder consulta = new StringBuilder();
		
		consulta.append("SELECT salario ")
		.append("FROM Salario salario ")
		.append("WHERE salario.data_pagamento_salario BETWEEN :data_inicio AND :data_fim");
	
		try {
			return manager.createQuery(consulta.toString(),Object.class)
						  .setParameter("data_inicio", data_inicio)
						  .setParameter("data_fim", data_fim).getResultList();
		} catch(NoResultException erro){
			return null;
		}
		
	}

}
