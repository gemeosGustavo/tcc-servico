package com.tcc.sgfr.domain.resource;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tcc.sgfr.domain.model.Missao;
import com.tcc.sgfr.domain.repository.MissaoRepository;


@RestController
@RequestMapping("/missao") // rota de acesso
public class MissaoResource {
	
	@Autowired
	private MissaoRepository missaoRepository;

	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@PostMapping(path = "/adicionar")
	public ResponseEntity<Missao> addMissao(@Valid @RequestBody Missao missao) {
		Missao missaonew = missaoRepository.save(missao);
		return ResponseEntity.status(HttpStatus.CREATED).body(missaonew);
	}

	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@GetMapping(value = "/dados")
	public ResponseEntity<List<Missao>> getMissao() {
		List<Missao> getAllMissao = missaoRepository.findAll();
		if (getAllMissao.isEmpty()) {
			return new ResponseEntity<List<Missao>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Missao>>(getAllMissao, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@GetMapping(value = "/dados/{id}")
	public ResponseEntity<Missao> getMissao(@PathVariable("id") Long id) {
		Missao getMissao = missaoRepository.findOne(id);
		if (getMissao == null) {	
			System.out.println("Missao com id " + id + "não encontrado");
			return new ResponseEntity<Missao>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Missao>(getMissao, HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@GetMapping(value = "/relatorio_missao/data_inicio={data_inicio}/data_fim={data_fim}")
	public ResponseEntity<Object> getMissaoRelatorio(
			@PathVariable("data_inicio") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate data_inicio,
			@PathVariable("data_fim") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate data_fim) {
		
		Object relatorioMissoes = missaoRepository.valorMissao(data_inicio, data_fim);
		if (relatorioMissoes == null) {
		
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Object>(relatorioMissoes, HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@GetMapping(value = "/relatorio_missao/total/data_inicio={data_inicio}/data_fim={data_fim}")
	public ResponseEntity<List<Object>>getMissoesRelatorios(
			@PathVariable("data_inicio") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate data_inicio,
			@PathVariable("data_fim") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate data_fim) {
		
		List<Object> relatorioMissoes = missaoRepository.missoes(data_inicio, data_fim);
		if (relatorioMissoes == null) {
		
			return new ResponseEntity<List<Object>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<Object>>(relatorioMissoes, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@PutMapping(value = "/atualizar/{id}")
	public ResponseEntity<Missao> updateMissao(@PathVariable("id") Long id, @Valid @RequestBody Missao missao) {
		System.out.println("Procurando Missao de id " + id);

		Missao missaoAtual = missaoRepository.findOne(id);

		if (missaoAtual == null) {
			System.out.println("Missao com id " + id + " não encontrado");
			return new ResponseEntity<Missao>(HttpStatus.NOT_FOUND);
		}
		BeanUtils.copyProperties(missao, missaoAtual, "id"); // observacao
		missaoAtual.setData_oferta_missao(missaoAtual.getData_oferta_missao());
		missaoAtual.setValor_missao(missaoAtual.getValor_missao());
		missaoRepository.save(missao);
		return new ResponseEntity<Missao>(missao, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('ADMIN')")
	@DeleteMapping(value = "/deletar/{id}")
	public ResponseEntity<Missao> deleteMissao(@PathVariable("id") long id) {
		System.out.println("Procurando Missao de id " + id);
		Missao deletarMissao = missaoRepository.findOne(id);
		if (deletarMissao == null) {
			System.out.println("Não foi possivel deletar a missão com id: " + id + ", id não encontrado");
			return new ResponseEntity<Missao>(HttpStatus.NOT_FOUND);
		}
		missaoRepository.delete(id);
		return new ResponseEntity<Missao>(HttpStatus.NO_CONTENT);
	}

}
