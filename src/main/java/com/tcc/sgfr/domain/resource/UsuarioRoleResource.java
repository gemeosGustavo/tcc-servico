package com.tcc.sgfr.domain.resource;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RestController;

import com.tcc.sgfr.domain.model.Role;
import com.tcc.sgfr.domain.model.Usuario;
import com.tcc.sgfr.domain.model.UsuarioRole;
import com.tcc.sgfr.domain.repository.RoleRepository;
import com.tcc.sgfr.domain.repository.UsuarioRepository;
import com.tcc.sgfr.domain.repository.UsuarioRoleRepository;

@RestController
public class UsuarioRoleResource {
	
	private UsuarioRepository usuarioRepository;
	private RoleRepository roleRepository;
	
	private UsuarioRoleRepository usuarioRoleRepository;
	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('ADMIN')")
	@PostMapping(path = "/addrole/{usuario}/{role}")
	public ResponseEntity<UsuarioRole> addRole(@PathVariable("id") Long idusuario, @PathVariable("id") Long idrole,
		@Valid @RequestBody UsuarioRole usuarioRole) {
		
		Usuario usuario = usuarioRepository.findOne(idusuario);
		Role role = roleRepository.findOne(idrole);
	
		UsuarioRole newPermission = new UsuarioRole(usuario, role);
		usuarioRoleRepository.save(newPermission);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(newPermission);
		
	}
	

}
