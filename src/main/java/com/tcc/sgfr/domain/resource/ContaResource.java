package com.tcc.sgfr.domain.resource;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tcc.sgfr.domain.model.Conta;
import com.tcc.sgfr.domain.repository.ContaRepository;

@RestController
@RequestMapping("/conta") // rota de acesso
public class ContaResource {
	
	@Autowired
	private ContaRepository contaRepository;
	
	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@PostMapping(path = "/adicionar")
	public ResponseEntity<Conta> addConta(@Valid @RequestBody Conta conta) {
		Conta contanew = contaRepository.save(conta);
		return ResponseEntity.status(HttpStatus.CREATED).body(contanew);
	}
	
	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@GetMapping(value = "/dados")
	public ResponseEntity<List<Conta>> getConta() {
		List<Conta> getAllConta = contaRepository.findAll();
		if (getAllConta.isEmpty()) {
			return new ResponseEntity<List<Conta>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Conta>>(getAllConta, HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@GetMapping(value = "/dados/{id}")
	public ResponseEntity<Conta> getConta(@PathVariable("id") Long id) {
		Conta getConta = contaRepository.findOne(id);
		if (getConta == null) {
			System.out.println("Conta com id " + id + "não encontrado");
			return new ResponseEntity<Conta>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Conta>(getConta, HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@GetMapping(value = "/relatorio_conta/data_inicio={data_inicio}/data_fim={data_fim}")
	public ResponseEntity<Object>getContaRelatorio(
			@PathVariable("data_inicio") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate data_inicio,
			@PathVariable("data_fim") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate data_fim) {
		
		Object relatorioContas = contaRepository.valorConta(data_inicio, data_fim);
		if (relatorioContas == null) {
		
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Object>(relatorioContas, HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@GetMapping(value = "/relatorio_conta/total/data_inicio={data_inicio}/data_fim={data_fim}")
	public ResponseEntity<List<Object>>getContasRelatorios(
			@PathVariable("data_inicio") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate data_inicio,
			@PathVariable("data_fim") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate data_fim) {
		
		List<Object> relatorioContas = contaRepository.contas(data_inicio, data_fim);
		if (relatorioContas == null) {
		
			return new ResponseEntity<List<Object>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<Object>>(relatorioContas, HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@PutMapping(value = "/atualizar/{id}")
	public ResponseEntity<Conta> updateGpssVacina(@PathVariable("id") Long id, @Valid @RequestBody Conta conta) {
		System.out.println("Procurando Conta de id " + id);

		Conta contaAtual = contaRepository.findOne(id);

		if (contaAtual == null) {
			System.out.println("Conta com id " + id + " não encontrado");
			return new ResponseEntity<Conta>(HttpStatus.NOT_FOUND);
		}
		BeanUtils.copyProperties(conta, contaAtual, "id"); // observacao
		contaAtual.setValor_conta(contaAtual.getValor_conta());
		contaAtual.setData_pagamento(contaAtual.getData_pagamento());
		contaAtual.setDescricao(contaAtual.getDescricao());
		
		contaRepository.save(conta);
		return new ResponseEntity<Conta>(conta, HttpStatus.OK);
	}
	
	
	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('ADMIN')")
	@DeleteMapping(value = "/deletar/{id}")
	public ResponseEntity<Conta> deleteConta(@PathVariable("id") long id) {
		System.out.println("Procurando Vacina de id " + id);

		Conta deletarConta = contaRepository.findOne(id);
		if (deletarConta == null) {
			System.out.println("Não foi possivel deletar conta com id: " + id + ", id não encontrado");
			return new ResponseEntity<Conta>(HttpStatus.NOT_FOUND);
		}
		contaRepository.delete(id);
		return new ResponseEntity<Conta>(HttpStatus.NO_CONTENT);
	}

}