package com.tcc.sgfr.domain.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tcc.sgfr.domain.model.Role;
import com.tcc.sgfr.domain.model.Usuario;
import com.tcc.sgfr.domain.model.UsuarioRole;
import com.tcc.sgfr.domain.repository.RoleRepository;
import com.tcc.sgfr.domain.repository.UsuarioRoleRepository;
import com.tcc.sgfr.security.services.UserService;


@RestController
@RequestMapping("/usuario") 
public class UsuarioResource {

	@Autowired
	private UserService userService;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private UsuarioRoleRepository usuarioRoleRepository;
	
	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('ADMIN')")
	@PostMapping(path = "/singup")
	public ResponseEntity<Usuario> addUsuario(@RequestBody Usuario usuario) {
		Usuario usuarioNew = userService.save(usuario);
		System.out.println("Usuário Salvo");
		System.out.println(usuarioNew.getPermissao());
		
		if (usuarioNew.getPermissao() == 1) {
			long permissao = 1;
			Role idRole = roleRepository.findOne(permissao);
			UsuarioRole userRole = new UsuarioRole(usuarioNew, idRole);
			usuarioRoleRepository.save(userRole);
		}
		else if (usuarioNew.getPermissao() == 2) {
			System.out.println("Entrou no if");
			long permissao = 2;
			Role idRole = roleRepository.findOne(permissao);
			System.out.println(idRole);
			UsuarioRole userRole = new UsuarioRole(usuarioNew, idRole);
			System.out.println("Atribuiu permissão");
			usuarioRoleRepository.save(userRole);
		}
		
		return ResponseEntity.status(HttpStatus.CREATED).body(usuarioNew);
	}
	
	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('ADMIN')")
	@GetMapping(value = "/dados")
	public ResponseEntity<List<Usuario>> getUsuario() {
		List<Usuario> getAllUsuario = userService.findAll();
		if (getAllUsuario.isEmpty()) {
			return new ResponseEntity<List<Usuario>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Usuario>>(getAllUsuario, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('ADMIN')")
	@GetMapping(value = "/dados/{id}")
	public ResponseEntity<Usuario> getUsuario(@PathVariable("id") Long id) {
		Usuario getUsuario = userService.findById(id);
		if (getUsuario == null) {	
			System.out.println("Oferta com id " + id + "não encontrado");
			return new ResponseEntity<Usuario>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Usuario>(getUsuario, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('ADMIN')")
	@PutMapping(value = "/atualizar/{id}")
	public ResponseEntity<Usuario> updateUsuario(@PathVariable("id") Long id, @Valid @RequestBody Usuario usuario) {
		System.out.println("Procurando usuario de id " + id);

		Usuario usuarioAtual = userService.findById(id);

		if (usuarioAtual == null) {
			System.out.println("Salario com id " + id + " não encontrado");
			return new ResponseEntity<Usuario>(HttpStatus.NOT_FOUND);
		}
		usuarioAtual.setNome(usuarioAtual.getNome());
		usuarioAtual.setSobrenome(usuarioAtual.getSobrenome());
		userService.save(usuario);
		return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
	}
}
