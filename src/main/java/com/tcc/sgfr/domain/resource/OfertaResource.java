package com.tcc.sgfr.domain.resource;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tcc.sgfr.domain.model.Oferta;
import com.tcc.sgfr.domain.repository.OfertaRepository;



@RestController
@RequestMapping("/oferta") // rota de acesso
public class OfertaResource {
	
	@Autowired
	private OfertaRepository ofertaRepository;
	
	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@PostMapping(path = "/adicionar")
	public ResponseEntity<Oferta> addOferta(@Valid @RequestBody Oferta oferta) {
		Oferta ofertanew = ofertaRepository.save(oferta);
		return ResponseEntity.status(HttpStatus.CREATED).body(ofertanew);
	}

	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@GetMapping(value = "/dados")
	public ResponseEntity<List<Oferta>> getOferta() {
		List<Oferta> getAllOferta = ofertaRepository.findAll();
		if (getAllOferta.isEmpty()) {
			return new ResponseEntity<List<Oferta>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Oferta>>(getAllOferta, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@GetMapping(value = "/dados/{id}")
	public ResponseEntity<Oferta> getOferta(@PathVariable("id") Long id) {
		Oferta getOferta = ofertaRepository.findOne(id);
		if (getOferta == null) {	
			System.out.println("Oferta com id " + id + "não encontrado");
			return new ResponseEntity<Oferta>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Oferta>(getOferta, HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@GetMapping(value = "/relatorio_oferta/data_inicio={data_inicio}/data_fim={data_fim}")
	public ResponseEntity<Object> getOfertaRelatorio(
			@PathVariable("data_inicio") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate data_inicio,
			@PathVariable("data_fim") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate data_fim) {
		
		Object relatorioOfertas = ofertaRepository.valorOferta(data_inicio, data_fim);
		if (relatorioOfertas == null) {
		
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Object>(relatorioOfertas, HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@GetMapping(value = "/relatorio_oferta/total/data_inicio={data_inicio}/data_fim={data_fim}")
	public ResponseEntity<List<Object>>getOfertasRelatorios(
			@PathVariable("data_inicio") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate data_inicio,
			@PathVariable("data_fim") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate data_fim) {
		
		List<Object> relatorioOfertas = ofertaRepository.ofertas(data_inicio, data_fim);
		if (relatorioOfertas == null) {
		
			return new ResponseEntity<List<Object>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<Object>>(relatorioOfertas, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@PutMapping(value = "/atualizar/{id}")
	public ResponseEntity<Oferta> updateOferta(@PathVariable("id") Long id, @Valid @RequestBody Oferta oferta) {
		System.out.println("Procurando Oferta de id " + id);

		Oferta ofertaAtual = ofertaRepository.findOne(id);

		if (ofertaAtual == null) {
			System.out.println("Oferta com id " + id + " não encontrado");
			return new ResponseEntity<Oferta>(HttpStatus.NOT_FOUND);
		}
		BeanUtils.copyProperties(oferta, ofertaAtual, "id"); // observacao
		ofertaAtual.setData_oferta(ofertaAtual.getData_oferta());
		ofertaAtual.setValor_oferta(ofertaAtual.getValor_oferta());
		ofertaRepository.save(oferta);
		return new ResponseEntity<Oferta>(oferta, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('ADMIN')")
	@DeleteMapping(value = "/deletar/{id}")
	public ResponseEntity<Oferta> deleteOferta(@PathVariable("id") long id) {
		System.out.println("Procurando Oferta de id " + id);
		Oferta deletarOferta = ofertaRepository.findOne(id);
		if (deletarOferta == null) {
			System.out.println("Não foi possivel deletar a oferta com id: " + id + ", id não encontrado");
			return new ResponseEntity<Oferta>(HttpStatus.NOT_FOUND);
		}
		ofertaRepository.delete(id);
		return new ResponseEntity<Oferta>(HttpStatus.NO_CONTENT);
	}

}
