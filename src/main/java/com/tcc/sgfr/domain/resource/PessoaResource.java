package com.tcc.sgfr.domain.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tcc.sgfr.domain.model.Pessoa;
import com.tcc.sgfr.domain.repository.PessoaRepository;


@RestController
@RequestMapping("/pessoa") // rota de acesso
public class PessoaResource {
	
	@Autowired
	private PessoaRepository pessoaRepository;

	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@PostMapping(path = "/adicionar")
	public ResponseEntity<Pessoa> addPessoa(@Valid @RequestBody Pessoa pessoa) {
		Pessoa pessoanew = pessoaRepository.save(pessoa);
		return ResponseEntity.status(HttpStatus.CREATED).body(pessoanew);
	}

	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@GetMapping(value = "/dados")
	public ResponseEntity<List<Pessoa>> getPessoa() {
		List<Pessoa> getAllPessoa = pessoaRepository.findAll();
		if (getAllPessoa.isEmpty()) {
			return new ResponseEntity<List<Pessoa>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Pessoa>>(getAllPessoa, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@GetMapping(value = "/dados/{id}")
	public ResponseEntity<Pessoa> getPessoa(@PathVariable("id") Long id) {
		Pessoa getPessoa = pessoaRepository.findOne(id);
		if (getPessoa == null) {	
			System.out.println("Oferta com id " + id + "não encontrado");
			return new ResponseEntity<Pessoa>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Pessoa>(getPessoa, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@PutMapping(value = "/atualizar/{id}")
	public ResponseEntity<Pessoa> updatePessoa(@PathVariable("id") Long id, @Valid @RequestBody Pessoa pessoa) {
		System.out.println("Procurando Pessoa de id " + id);

		Pessoa pessoaAtual = pessoaRepository.findOne(id);

		if (pessoaAtual == null) {
			System.out.println("Pessoa com id " + id + " não encontrado");
			return new ResponseEntity<Pessoa>(HttpStatus.NOT_FOUND);
		}
		BeanUtils.copyProperties(pessoa, pessoaAtual, "id"); // observacao
		pessoaAtual.setNome(pessoaAtual.getNome());
		pessoaAtual.setTelefone(pessoaAtual.getTelefone());
		pessoaAtual.setData_nascimento(pessoaAtual.getData_nascimento());
		pessoaAtual.setEnderecoid(pessoaAtual.getEnderecoid());
		pessoaAtual.setNumero_casa(pessoaAtual.getNumero_casa());
		pessoaRepository.save(pessoa);
		return new ResponseEntity<Pessoa>(pessoa, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('ADMIN')")
	@DeleteMapping(value = "/deletar/{id}")
	public ResponseEntity<Pessoa> deletePessoa(@PathVariable("id") long id) {
		System.out.println("Procurando Pessoa de id " + id);
		Pessoa deletarPessoa = pessoaRepository.findOne(id);
		if (deletarPessoa == null) {
			System.out.println("Não foi possivel deletar a pessoa com id: " + id + ", id não encontrado");
			return new ResponseEntity<Pessoa>(HttpStatus.NOT_FOUND);
		}
		pessoaRepository.delete(id);
		return new ResponseEntity<Pessoa>(HttpStatus.NO_CONTENT);
	}


}
