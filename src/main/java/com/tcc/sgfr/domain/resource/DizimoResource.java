package com.tcc.sgfr.domain.resource;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tcc.sgfr.domain.model.Dizimo;
import com.tcc.sgfr.domain.repository.DizimoRepository;

@RestController
@RequestMapping("/dizimo") // rota de acesso
public class DizimoResource {

	@Autowired
	private DizimoRepository dizimoRepository;
	
	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@PostMapping(path = "/adicionar")
	public ResponseEntity<Dizimo> addDizmo(@Valid @RequestBody Dizimo dizimo) {
		Dizimo dizimonew = dizimoRepository.save(dizimo);
		return ResponseEntity.status(HttpStatus.CREATED).body(dizimonew);
	}
	
	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@GetMapping(value = "/dados")
	public ResponseEntity<List<Dizimo>> getDizimo() {
		List<Dizimo> getAllDizimo = dizimoRepository.findAll();
		if (getAllDizimo.isEmpty()) {
			return new ResponseEntity<List<Dizimo>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Dizimo>>(getAllDizimo, HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@GetMapping(value = "/dados/{id}")
	public ResponseEntity<Dizimo> getDizimo(@PathVariable("id") Long id) {
		Dizimo getDizimo = dizimoRepository.findOne(id);
		if (getDizimo == null) {	
			System.out.println("Dizimo com id " + id + "não encontrado");
			return new ResponseEntity<Dizimo>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Dizimo>(getDizimo, HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@GetMapping(value = "/relatorio_dizimo/data_inicio={data_inicio}/data_fim={data_fim}")
	public ResponseEntity<Object> getDizimoRelatorio(
			@PathVariable("data_inicio") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate data_inicio,
			@PathVariable("data_fim") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate data_fim) {
		
		Object relatorioDizimos = dizimoRepository.valorDizimo(data_inicio, data_fim);
		if (relatorioDizimos == null) {
		
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Object>(relatorioDizimos, HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@GetMapping(value = "/relatorio_dizimo/total/data_inicio={data_inicio}/data_fim={data_fim}")
	public ResponseEntity<List<Object>>getDizimosRelatorios(
			@PathVariable("data_inicio") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate data_inicio,
			@PathVariable("data_fim") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate data_fim) {
		
		List<Object> relatorioDizimos = dizimoRepository.dizimos(data_inicio, data_fim);
		if (relatorioDizimos == null) {
		
			return new ResponseEntity<List<Object>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<Object>>(relatorioDizimos, HttpStatus.OK);
	}
	
	
	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@PutMapping(value = "/atualizar/{id}")
	public ResponseEntity<Dizimo> updateGpssVacina(@PathVariable("id") Long id, @Valid @RequestBody Dizimo dizimo) {
		System.out.println("Procurando Dizimo de id " + id);

		Dizimo dizimoAtual = dizimoRepository.findOne(id);

		if (dizimoAtual == null) {
			System.out.println("Dizimo com id " + id + " não encontrado");
			return new ResponseEntity<Dizimo>(HttpStatus.NOT_FOUND);
		}
		BeanUtils.copyProperties(dizimo, dizimoAtual, "id"); // observacao
		dizimoAtual.setData_dizimo(dizimoAtual.getData_dizimo());	
		dizimoAtual.setValor_dizimo(dizimoAtual.getValor_dizimo());
		dizimoAtual.setPessoaid(dizimoAtual.getPessoaid());
		dizimoRepository.save(dizimo);
		return new ResponseEntity<Dizimo>(dizimo, HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('ADMIN')")
	@DeleteMapping(value = "/deletar/{id}")
	public ResponseEntity<Dizimo> deleteDizimo(@PathVariable("id") long id) {
		System.out.println("Procurando Dizimo de id " + id);
		Dizimo deletarDizimo = dizimoRepository.findOne(id);
		if (deletarDizimo == null) {
			System.out.println("Não foi possivel deletar dizimo com id: " + id + ", id não encontrado");
			return new ResponseEntity<Dizimo>(HttpStatus.NOT_FOUND);
		}
		dizimoRepository.delete(id);
		return new ResponseEntity<Dizimo>(HttpStatus.NO_CONTENT);
	}

}
