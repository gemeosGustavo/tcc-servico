package com.tcc.sgfr.domain.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tcc.sgfr.domain.model.Endereco;
import com.tcc.sgfr.domain.repository.EnderecoRepository;

@RestController
@RequestMapping("/endereco") // rota de acesso
public class EnderecoResource {

	@Autowired
	private EnderecoRepository enderecoRepository;

	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER') or hasRole('ADMIN')")
	@PostMapping(path = "/adicionar")
	public ResponseEntity<Endereco> addEndereco(@Valid @RequestBody Endereco endereco) {
		Endereco endereconew = enderecoRepository.save(endereco);
		return ResponseEntity.status(HttpStatus.CREATED).body(endereconew);
	}

	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@GetMapping(value = "/dados")
	public ResponseEntity<List<Endereco>> getEndereco() {

		List<Endereco> getAllEndereco = enderecoRepository.findAll();
		if (getAllEndereco.isEmpty()) {
			return new ResponseEntity<List<Endereco>>(HttpStatus.NO_CONTENT);
		}

		return new ResponseEntity<List<Endereco>>(getAllEndereco, HttpStatus.OK);

	}

	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@GetMapping(value = "/dados/{id}")
	public ResponseEntity<Endereco> getEndereco(@PathVariable("id") Long id) {
		Endereco getEndereco = enderecoRepository.findOne(id);
		if (getEndereco == null) {
			System.out.println("Dizimo com id " + id + "não encontrado");
			return new ResponseEntity<Endereco>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Endereco>(getEndereco, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@PutMapping(value = "/atualizar/{id}")
	public ResponseEntity<Endereco> updateEndereco(@PathVariable("id") Long id, @Valid @RequestBody Endereco endereco) {
		System.out.println("Procurando Endereço de id " + id);

		Endereco enderecoAtual = enderecoRepository.findOne(id);

		if (enderecoAtual == null) {
			System.out.println("Endereço com id " + id + " não encontrado");
			return new ResponseEntity<Endereco>(HttpStatus.NOT_FOUND);
		}
		BeanUtils.copyProperties(endereco, enderecoAtual, "id"); // observacao
		enderecoAtual.setLogradouro(enderecoAtual.getLogradouro());
		enderecoAtual.setBairro(enderecoAtual.getBairro());
		enderecoAtual.setCidade(enderecoAtual.getCidade());
		enderecoAtual.setUf(enderecoAtual.getUf());
		enderecoRepository.save(endereco);
		return new ResponseEntity<Endereco>(endereco, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('ADMIN')")
	@DeleteMapping(value = "/deletar/{id}")
	public ResponseEntity<Endereco> deleteEndereco(@PathVariable("id") long id) {
		System.out.println("Procurando Endereco de id " + id);
		Endereco deletarEndereco = enderecoRepository.findOne(id);
		if (deletarEndereco == null) {
			System.out.println("Não foi possivel deletar o endereço com id: " + id + ", id não encontrado");
			return new ResponseEntity<Endereco>(HttpStatus.NOT_FOUND);
		}
		enderecoRepository.delete(id);
		return new ResponseEntity<Endereco>(HttpStatus.NO_CONTENT);
	}

}
