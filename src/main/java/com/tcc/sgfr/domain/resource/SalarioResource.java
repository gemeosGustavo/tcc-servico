package com.tcc.sgfr.domain.resource;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tcc.sgfr.domain.model.Salario;
import com.tcc.sgfr.domain.repository.SalarioRepository;


@RestController
@RequestMapping("/salario") 
public class SalarioResource {

	@Autowired
	private SalarioRepository salarioRepository;
		
	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@PostMapping(path = "/adicionar")
	public ResponseEntity<Salario> addSalario(@Valid @RequestBody Salario salario) {
		Salario salarionew = salarioRepository.save(salario);
		return ResponseEntity.status(HttpStatus.CREATED).body(salarionew);
	}

	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@GetMapping(value = "/dados")
	public ResponseEntity<List<Salario>> getSalario() {
		List<Salario> getAllSalario = salarioRepository.findAll();
		if (getAllSalario.isEmpty()) {
			return new ResponseEntity<List<Salario>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Salario>>(getAllSalario, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@GetMapping(value = "/dados/{id}")
	public ResponseEntity<Salario> getSalario(@PathVariable("id") Long id) {
		Salario getSalario = salarioRepository.findOne(id);
		if (getSalario == null) {	
			System.out.println("Salario com id " + id + "não encontrado");
			return new ResponseEntity<Salario>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Salario>(getSalario, HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@GetMapping(value = "/relatorio_salario/data_inicio={data_inicio}/data_fim={data_fim}")
	public ResponseEntity<Object> getSalarioRelatorio(
			@PathVariable("data_inicio") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate data_inicio,
			@PathVariable("data_fim") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate data_fim) {
		
		Object relatorioSalarios = salarioRepository.valorPagamento(data_inicio, data_fim);
		if (relatorioSalarios == null) {
		
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Object>(relatorioSalarios, HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@GetMapping(value = "/relatorio_salario/total/data_inicio={data_inicio}/data_fim={data_fim}")
	public ResponseEntity<List<Object>>getSalariosRelatorios(
			@PathVariable("data_inicio") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate data_inicio,
			@PathVariable("data_fim") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate data_fim) {
		
		List<Object> relatorioSalarios = salarioRepository.salarios(data_inicio, data_fim);
		if (relatorioSalarios == null) {
		
			return new ResponseEntity<List<Object>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<Object>>(relatorioSalarios, HttpStatus.OK);
	}


	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@PutMapping(value = "/atualizar/{id}")
	public ResponseEntity<Salario> updateSalario(@PathVariable("id") Long id, @Valid @RequestBody Salario salario) {
		System.out.println("Procurando Salario de id " + id);

		Salario salarioAtual = salarioRepository.findOne(id);

		if (salarioAtual == null) {
			System.out.println("Salario com id " + id + " não encontrado");
			return new ResponseEntity<Salario>(HttpStatus.NOT_FOUND);
		}
		BeanUtils.copyProperties(salario, salarioAtual, "id"); // observacao
		salarioAtual.setData_pagamento_salario(salarioAtual.getData_pagamento_salario());
		salarioAtual.setValor_pagamento(salarioAtual.getValor_pagamento());
		salarioAtual.setDescricao_pagamento(salarioAtual.getDescricao_pagamento());
		salarioAtual.setFuncionarioid(salarioAtual.getFuncionarioid());
		salarioRepository.save(salario);
		return new ResponseEntity<Salario>(salario, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('ADMIN')")
	@DeleteMapping(value = "/deletar/{id}")
	public ResponseEntity<Salario> deleteSalario(@PathVariable("id") long id) {
		System.out.println("Procurando Salario de id " + id);
		Salario deletarSalario = salarioRepository.findOne(id);
		if (deletarSalario == null) {
			System.out.println("Não foi possivel deletar salario com id: " + id + ", id não encontrado");
			return new ResponseEntity<Salario>(HttpStatus.NOT_FOUND);
		}
		salarioRepository.delete(id);
		return new ResponseEntity<Salario>(HttpStatus.NO_CONTENT);
	}
}
