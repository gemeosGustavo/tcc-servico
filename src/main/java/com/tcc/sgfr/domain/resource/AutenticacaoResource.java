package com.tcc.sgfr.domain.resource;

import javax.naming.AuthenticationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.ResponseEntity;

import com.tcc.sgfr.domain.model.AuthToken;
import com.tcc.sgfr.domain.repository.UsuarioRepository;
import com.tcc.sgfr.security.jwt.JwtProvider;
import com.tcc.sgfr.security.model.LoginForm;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/auth")
public class AutenticacaoResource {
	
	@Autowired
	AuthenticationManager authenticationManager;
	 
	@Autowired
	UsuarioRepository usuarioRepository;
	 
	@Autowired
	JwtProvider jwtProvider;

	@CrossOrigin(origins = "*")
	@PostMapping(path = "/signin")
	public ResponseEntity<?> authenticateUser(@RequestBody LoginForm loginRequest) throws AuthenticationException {
	 
		
		final Authentication authentication = authenticationManager.authenticate(
			new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
		
	    SecurityContextHolder.getContext().setAuthentication(authentication);
	    
	    final String jwt = jwtProvider.generateToken(authentication);
	    final UserDetails userDetails = (UserDetails) authentication.getPrincipal();
	 
	    return ResponseEntity.ok(new AuthToken(jwt));
	  }
	

}
