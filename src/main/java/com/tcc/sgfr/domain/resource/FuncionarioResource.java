package com.tcc.sgfr.domain.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tcc.sgfr.domain.model.Funcionario;
import com.tcc.sgfr.domain.repository.FuncionarioRepository;



@RestController
@RequestMapping("/funcionario") // rota de acesso
public class FuncionarioResource {
	
	@Autowired
	private FuncionarioRepository funcionarioRepository;
	
	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@PostMapping(path = "/adicionar")
	public ResponseEntity<Funcionario> addFunconario(@Valid @RequestBody Funcionario funcionario) {
		Funcionario funcionarionew = funcionarioRepository.save(funcionario);
		return ResponseEntity.status(HttpStatus.CREATED).body(funcionarionew);
	}

	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@GetMapping(value = "/dados")
	public ResponseEntity<List<Funcionario>> getFuncionario() {
		List<Funcionario> getAllFuncionario = funcionarioRepository.findAll();
		if (getAllFuncionario.isEmpty()) {
			return new ResponseEntity<List<Funcionario>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Funcionario>>(getAllFuncionario, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@GetMapping(value = "/dados/{id}")
	public ResponseEntity<Funcionario> getFuncionario(@PathVariable("id") Long id) {
		Funcionario getFuncionario = funcionarioRepository.findOne(id);
		if (getFuncionario == null) {	
			System.out.println("Funcionario com id " + id + "não encontrado");
			return new ResponseEntity<Funcionario>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Funcionario>(getFuncionario, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	@PutMapping(value = "/atualizar/{id}")
	public ResponseEntity<Funcionario> updateFuncionario(@PathVariable("id") Long id, @Valid @RequestBody Funcionario funcionario) {
		System.out.println("Procurando Funcionario de id " + id);

		Funcionario funcionarioAtual = funcionarioRepository.findOne(id);

		if (funcionarioAtual == null) {
			System.out.println("Endereço com id " + id + " não encontrado");
			return new ResponseEntity<Funcionario>(HttpStatus.NOT_FOUND);
		}
		BeanUtils.copyProperties(funcionario, funcionarioAtual, "id"); // observacao
		funcionarioAtual.setCargo(funcionarioAtual.getCargo());
		funcionarioAtual.setSalario(funcionarioAtual.getSalario());
		funcionarioAtual.setPessoaid(funcionarioAtual.getPessoaid());
		funcionarioRepository.save(funcionario);
		return new ResponseEntity<Funcionario>(funcionario, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@PreAuthorize("hasAnyRole('ADMIN')")
	@DeleteMapping(value = "/deletar/{id}")
	public ResponseEntity<Funcionario> deleteFuncionario(@PathVariable("id") long id) {
		System.out.println("Procurando Funcionario de id " + id);
		Funcionario deletarFuncionario = funcionarioRepository.findOne(id);
		if (deletarFuncionario == null) {
			System.out.println("Não foi possivel deletar o funcionario com id: " + id + ", id não encontrado");
			return new ResponseEntity<Funcionario>(HttpStatus.NOT_FOUND);
		}
		funcionarioRepository.delete(id);
		return new ResponseEntity<Funcionario>(HttpStatus.NO_CONTENT);
	}

}
