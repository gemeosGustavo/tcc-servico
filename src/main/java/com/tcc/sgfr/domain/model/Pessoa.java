package com.tcc.sgfr.domain.model;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "pessoa")
public class Pessoa {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
	@Column(name = "nome")   		// referencia da coluna no json - nao por letra maiuscula
	private String nome;
	
	@Column(name = "telefone")   		
	private String telefone;
	
	@Column(name = "data_nascimento")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate data_nascimento;
	
	@Column(name = "numero_casa")
	private int numero_casa;
	
	@ManyToOne
	@JoinColumn(name = "enderecoid")
	private Endereco enderecoid;
	
	@Column(name="excluido")
	private int excluido;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public LocalDate getData_nascimento() {
		return data_nascimento;
	}

	public void setData_nascimento(LocalDate data_nascimento) {
		this.data_nascimento = data_nascimento;
	}

	public int getNumero_casa() {
		return numero_casa;
	}

	public void setNumero_casa(int numero_casa) {
		this.numero_casa = numero_casa;
	}

	public Endereco getEnderecoid() {
		return enderecoid;
	}

	public void setEnderecoid(Endereco enderecoid) {
		this.enderecoid = enderecoid;
	}

	public int getExcluido() {
		return excluido;
	}

	public void setExcluido(int excluido) {
		this.excluido = excluido;
	}

	@Override
	public String toString() {
		return "Pessoa [id=" + id + ", nome=" + nome + ", telefone=" + telefone + ", data_nascimento=" + data_nascimento
				+ ", numero_casa=" + numero_casa + ", enderecoid=" + enderecoid + ", excluido=" + excluido + "]";
	}
	
	
}
