package com.tcc.sgfr.domain.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;


@Entity
@Table(name = "dizimo")
public class Dizimo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
	@Column(name = "valor_dizimo")   		// referencia da coluna no banco de dados
	private double valor_dizimo;

	@Column(name = "data_dizimo")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate data_dizimo;
	
	@ManyToOne
	@JoinColumn(name = "pessoaid")
	private Pessoa pessoaid;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getValor_dizimo() {
		return valor_dizimo;
	}

	public void setValor_dizimo(double valor_dizimo) {
		this.valor_dizimo = valor_dizimo;
	}

	public LocalDate getData_dizimo() {
		return data_dizimo;
	}

	public void setData_dizimo(LocalDate data_dizimo) {
		this.data_dizimo = data_dizimo;
	}

	public Pessoa getPessoaid() {
		return pessoaid;
	}

	public void setPessoaid(Pessoa pessoaid) {
		this.pessoaid = pessoaid;
	}

	@Override
	public String toString() {
		return "Dizimo [id=" + id + ", valor_dizimo=" + valor_dizimo + ", data_dizimo=" + data_dizimo + ", pessoaid="
				+ pessoaid + "]";
	}
	
}