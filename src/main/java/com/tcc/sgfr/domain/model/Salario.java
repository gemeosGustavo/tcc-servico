package com.tcc.sgfr.domain.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table
public class Salario {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;

	@Column(name = "valor_pagamento") // referencia da coluna no json - nao por letra maiuscula
	private double valor_pagamento;

	@Column(name = "data_pagamento_salario")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate data_pagamento_salario;
		
	@Column(name = "descricao_pagamento")
	private String descricao_pagamento;
	
	@ManyToOne
	@JoinColumn(name = "funcionarioid")
	private Pessoa funcionarioid;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getValor_pagamento() {
		return valor_pagamento;
	}

	public void setValor_pagamento(double valor_pagamento) {
		this.valor_pagamento = valor_pagamento;
	}

	public LocalDate getData_pagamento_salario() {
		return data_pagamento_salario;
	}

	public void setData_pagamento_salario(LocalDate data_pagamento_salario) {
		this.data_pagamento_salario = data_pagamento_salario;
	}

	public String getDescricao_pagamento() {
		return descricao_pagamento;
	}

	public void setDescricao_pagamento(String descricao_pagamento) {
		this.descricao_pagamento = descricao_pagamento;
	}

	public Pessoa getFuncionarioid() {
		return funcionarioid;
	}

	public void setFuncionarioid(Pessoa funcionarioid) {
		this.funcionarioid = funcionarioid;
	}

	@Override
	public String toString() {
		return "Salario [id=" + id + ", valor_pagamento=" + valor_pagamento + ", data_pagamento_salario="
				+ data_pagamento_salario + ", descricao_pagamento=" + descricao_pagamento + ", funcionarioid="
				+ funcionarioid + "]";
	}

}
