package com.tcc.sgfr.domain.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "oferta")
public class Oferta {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
	@Column(name = "valor_oferta")   		// referencia da coluna no banco - nao por letra maiuscula
	private double valor_oferta;

	@Column(name = "data_oferta")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate data_oferta;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getValor_oferta() {
		return valor_oferta;
	}

	public void setValor_oferta(double valor_oferta) {
		this.valor_oferta = valor_oferta;
	}

	public LocalDate getData_oferta() {
		return data_oferta;
	}

	public void setData_oferta(LocalDate data_oferta) {
		this.data_oferta = data_oferta;
	}

	@Override
	public String toString() {
		return "Oferta [id=" + id + ", valor_oferta=" + valor_oferta + ", data_oferta=" + data_oferta + "]";
	}

	
}
