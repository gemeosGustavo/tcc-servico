package com.tcc.sgfr.domain.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "missao")
public class Missao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
	@Column(name = "valor_missao")   		// referencia da coluna no json - nao por letra maiuscula
	private double valor_missao;

	@Column(name = "data_oferta_missao")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate data_oferta_missao;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getValor_missao() {
		return valor_missao;
	}

	public void setValor_missao(double valor_missao) {
		this.valor_missao = valor_missao;
	}

	public LocalDate getData_oferta_missao() {
		return data_oferta_missao;
	}

	public void setData_oferta_missao(LocalDate data_oferta_missao) {
		this.data_oferta_missao = data_oferta_missao;
	}

	@Override
	public String toString() {
		return "Missao [id=" + id + ", valor_missao=" + valor_missao + ", data_oferta_missao=" + data_oferta_missao
				+ "]";
	}
	
	
}
	
	