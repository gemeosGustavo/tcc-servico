package com.tcc.sgfr.domain.model;

public enum  RoleName {
    USER,
    ADMIN
}