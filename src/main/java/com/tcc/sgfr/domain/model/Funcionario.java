package com.tcc.sgfr.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "funcionario")
public class Funcionario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
	@Column(name = "cargo")   		// referencia da coluna no json - nao por letra maiuscula
	private String cargo;

	@Column(name = "salario")
	private double salario;
	
	@ManyToOne
	@JoinColumn(name = "pessoaid")
	private Pessoa pessoaid;

	
	//GET E SET - (ALT + S + R)

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

	public Pessoa getPessoaid() {
		return pessoaid;
	}

	public void setPessoaid(Pessoa pessoaid) {
		this.pessoaid = pessoaid;
	}

	
	@Override
	public String toString() {
		return "Funcionario [id=" + id + ", cargo=" + cargo + ", salario=" + salario + ", pessoaid=" + pessoaid
				+ "]";
	}
	
	
	// HASHCOD (ALT + S + H + H)
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Funcionario other = (Funcionario) obj;
		if (id != other.id)
			return false;
		return true;
	}

}