package com.tcc.sgfr.domain.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "conta")
public class Conta {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;

	@Column(name = "valor_conta") // referencia da coluna no banco de dados -
	private double valor_conta;

	@Column(name = "data_pagamento")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate data_pagamento;
	
	@Column(name = "descricao")
	private String descricao;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getValor_conta() {
		return valor_conta;
	}

	public void setValor_conta(double valor_conta) {
		this.valor_conta = valor_conta;
	}

	public LocalDate getData_pagamento() {
		return data_pagamento;
	}

	public void setData_pagamento(LocalDate data_pagamento) {
		this.data_pagamento = data_pagamento;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public String toString() {
		return "Conta [id=" + id + ", valor_conta=" + valor_conta + ", data_pagamento=" + data_pagamento
				+ ", descricao=" + descricao + "]";
	}


	
}
